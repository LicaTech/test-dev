# Test Developpeur Front End

## Instructions
- Le projet devra être développé avec React et on devra utiliser les composants de [MUI](https://mui.com) qui implémentent Material UI le plus possible.

- Ci joint les screenshots suivants :
1. [Mon profil screen 1](https://gitlab.com/LicaTech/test-dev/-/blob/main/Screen_Shot_2022-11-23_at_4.31.09_PM.png)
1. [Mon profil screen 2](https://gitlab.com/LicaTech/test-dev/-/blob/main/Screen_Shot_2022-11-23_at_4.31.16_PM.png)
1. [La section mes documents redirige sur cette vue](https://gitlab.com/LicaTech/test-dev/-/blob/main/Screen_Shot_2022-11-23_at_4.39.30_PM.png)


- Il s'agit d'une vue des informations de profil d'un participant. On peut voir que les différentes informations sont organisées selon des rubriques / cards à savoir **Image**, **Coordonnées**, **Informations générales**, **Sécurité**, **Adresse** et la rubrique **Mes Documents** qui redirige sur une autre page. Les maquettes sont données a titre d'information mais on souhaite re-organiser ces informations pour les rendre plus lisibles/ user-friendly à l'utilisateur de la plateforme, donc il ne faut pas hésiter à faire des propositions alternatives. L'exercice consiste à proposer un design et à l'implémenter à l'aide de composants MUI le plus possible et completer par des composants customs seulement si nécessaire.

- L'UI devra etre compatible en vue mobile en suivant le principe du mobile first.

- Pour le choix des couleurs/du thème est libre, utiliser une palette Material, choix libre, faire juste en sorte que ce soit agréable à l'oeil ;).

- Tous les boutons des screenshots devront être présents, mais il n'est pas nécessaire d'implémenter l'action lors du click.

- Pour récupérer les donnees, utiliser les données dans le fichier [data.json](https://gitlab.com/LicaTech/test-dev/-/blob/main/data.json) qu'on chargera à l'aide d'une méthode [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).

- Le choix de l'architecture cote Front End est libre et devra pouvoir être expliquer lors de l'entretien :)

- Le code devra être poussée sur une branche nommée exercice-MesInitiales-DateRendu, exemple exercice-FI-23112022


- N'hesitez pas à envoyer un message à fabien@lica-europe.org en cas de question .


